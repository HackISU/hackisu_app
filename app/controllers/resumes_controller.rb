class ResumesController < ApplicationController
  def index
    @resumes = Resume.all
  end

  def new
    @resume = Resume.new
  end

  def create
    @resume = Resume.new(resume_params)

    if @resume.save
      redirect_to resumes_path, notice: "The resume #{@resume.name} has been uploaded."
    else
      render "new"
    end
  end

  def destroy
    @resume = Resume.find(params[:id])
    @resume.destroy
    redirect_to resumes_path, notice:  "The resume #{@resume.name} has been deleted."
  end

  def edit
    @resume = Resume.new(resume_params)
  end


private
  def resume_params
    params.require(:resume).permit(:name, :attachment)
  end

def Download
  @resume = Resume.new
  # render text: "Hello World"


end




  #send_file(
    #"#{Rails.root}/public/uploads/resume/attachment/2/helloworld.txt",
    #filename: "helloworld.txt",
    #type: "application/txt"
    #)
    #send_file '/public/uploads/resume/attachment/2/helloworld.txt'
end
