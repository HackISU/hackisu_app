class DiffViewerController < ApplicationController
  include DiffViewerHelper


  require 'diffy'

  def index
    # # @r = DiffViewer.new("FAKE_GIT")
    # s1 = File.read("FAKE_GIT/file.txt")
    # s2 = File.read("FAKE_GIT/file1.txt")
    # @d = Diffy::Diff.new(s1, s2).to_s(:html_simple)

    # @file = File.read("FAKE_GIT/file.txt")
    # @file1 = File.read("FAKE_GIT/file1.txt")

    @file = File.read("public/uploads/resume/attachment/string1.txt")
    @file1 = File.read("public/uploads/resume/attachment/string2.txt")

    @css = Diffy::CSS
    splitdiff = Diffy::SplitDiff.new(@file, @file1, :format => :html)

    @combined = Diffy::Diff.new(@file, @file1).to_s(:html_simple).html_safe
    @diffLeft = splitdiff.left.html_safe
    @diffRight = splitdiff.right.html_safe
  end

  def create
  end

  def destroy
  end
end
