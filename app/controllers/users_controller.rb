class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
  @user = User.new(user_params)
  # @user = User.new(params[:user])
    if @user.save
      flash[:success] = "Thanks for signing up, #{@user.username}! You may now log in."
      redirect_to login_url
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:username, :password, :password_confirmation)
    end
end